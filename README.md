# Table of contents

[![Daniel Caesar - Get you](https://img.youtube.com/vi/VID/0.jpg)](https://youtu.be/uQFVqltOXRg)

1.  [Folder structure and reasoning](docs/structure.md)
2.  [Unit tests](docs/unit-testing.md)
3.  Integration tests <<< Omitting, Mark's the typed system genius here.
4.  E2E tests <<< Omitting, gist is [Chimp](https://chimp.readme.io/), Page Object Model, Feature files, Step definitions