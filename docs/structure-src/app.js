import React from 'react'
import { Provider } from 'react-redux'
import Switch from 'react-router-dom/Switch'

// @TODO: migrate lib modules
import { localise } from './lib/localisation'
import { format as formatPrice } from './lib/price'

import core from './core'
import withRootSetup from './lib/withRootSetup'
import { getRoutes, renderRoute, renderRedirect } from './lib/routing'

/**
 * Application init
 * - called by a renderer (Browser, Server, ...)
 * - renders registered routes within registered root component
 *
 * @param {object} store - initialised store
 * @param {Component} Router - Renderer's router component
 */
export default ({ store, Router, ...routerProps }) => {
  const { config: {
    language, brandName, currencyCode
  } } = store.getState()

  const appContext = {
    localise: (...args) => localise(language, brandName, ...args),
    formatPrice: (...args) => formatPrice(currencyCode, ...args)
  }

  // Root component registered by main feature (./features/main)
  const Root = withRootSetup(core.get.rootComponent(), appContext)

  return (
    <Provider store={store}>
      <Router { ...routerProps }>
        <Root>
          <Switch>
          { getRoutes({ l: appContext.localise })
            .map((route) => route.to ?
              renderRedirect(route) :
              renderRoute(route))
          }
          </Switch>
        </Root>
      </Router>
    </Provider>
  )
}
