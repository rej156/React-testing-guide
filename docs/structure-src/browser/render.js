import path from 'ramda/src/path'
import { render } from 'react-dom'
import Router from 'react-router-dom/BrowserRouter'

import { setDictionary } from '../lib/localisation'
import * as sendReporter from './lib/reporter'
import hijack from './lib/hijack-links'

import getApp from '../app'
import core from '../core'
import { initStore } from '../store'
import { bootstrapFeatures } from '../bootstrap'

sendReporter.start()

if (window.isRedAnt) hijack((pathname) => window.location = pathname)

/**
 * Bootstrap features for browser
 * - brought to you by ... Webpack for require.context + DefinePlugin for REGEXP_APP_FEATURES
 */
const requireFeature = require.context('../features', true, REGEXP_APP_FEATURES)
bootstrapFeatures((feature) => requireFeature(`./${feature}/index.js`))

/**
 * @TODO
 * implement getPersistedState from localStorage
 * this should be set via redux store enhancer using core.get.persistedStateKeys()
 * @example
 *  const getPersistedState = () => JSON.parse(localStorage.getItem('@monty/state'))
 *  const initialState = { ...window.__INITIAL_STATE__, ...getPersistedState() }
 */
const initialState = window.__INITIAL_STATE__
core.register.initialState(initialState)

const dictionary = path(['localisation', 'dictionary'], initialState)
if (dictionary) setDictionary(dictionary)

const store = initStore()
const rootEl = document.getElementById('root')

render(getApp({ store, Router }), rootEl, () => {
  rootEl.classList.remove('nojs')
})

window.__qubitStore = store
