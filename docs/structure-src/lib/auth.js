import path from 'ramda/src/path'
import core from '../core'

export const isAuth = () => !!path(['auth', 'authentication'], core.get.store().getState())

export const isAnon = () => path(['routing', 'location', 'query', 'isAnonymous'], core.get.store().getState()) === 'true'
