import React from 'react'
import Redirect from 'react-router-dom/Redirect'

import * as auth from '../lib/auth'
import getDisplayName from './getDisplayName'

export default (ComponentWithAuthCheck, {
  redirectTo = '/login',
  whenAuth = false
} = {}) => {
  const WithAuthCheck = (props) => {
    const isAuth = auth.isAuth()

    return (isAuth && whenAuth) || (!isAuth && !whenAuth) ?
      <Redirect to={{ pathname: redirectTo, state: { from: props.location } }} /> :
      <ComponentWithAuthCheck {...props} />
  }

  WithAuthCheck.displayName = `withAuthCheck(${getDisplayName(ComponentWithAuthCheck)})`
  WithAuthCheck.WrappedComponent = ComponentWithAuthCheck.WrappedComponent || ComponentWithAuthCheck

  return WithAuthCheck
}
