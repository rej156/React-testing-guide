import React from 'react'

if (process.browser) require('../../../../src/shared/components/containers/FixedHeightPage/FixedHeightPage.css')

export default (ComponentWithFixedHeight) => (props) => (
  <div className="FixedHeightPage">
    <ComponentWithFixedHeight {...props} />
  </div>
)
