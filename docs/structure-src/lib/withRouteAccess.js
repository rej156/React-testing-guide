import React, { Component } from 'react'
import pick from 'ramda/src/pick'

import core from '../core'
import getDisplayName from './getDisplayName'

export default (ComponentWithRouteAccess, { onEnter, onLeave }) => {
  return class RouteAccess extends Component {
    static displayName = `WithRouteAccess(${getDisplayName(ComponentWithRouteAccess)})`
    static WrappedComponent = ComponentWithRouteAccess.WrappedComponent || ComponentWithRouteAccess

    componentWillMount() {
      if (typeof onEnter === 'function') {
        onEnter(...this.getArgs())
      }
    }

    componentWillUnmount() {
      if (typeof onLeave === 'function') {
        onLeave(...this.getArgs())
      }
    }

    getArgs() {
      return [
        pick(['location', 'replace'], this.props),
        core.get.store()
      ]
    }

    render() {
      return <ComponentWithRouteAccess { ...this.props} />
    }
  }
}
