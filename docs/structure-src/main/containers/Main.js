import Main
  from '../../../../../../src/shared/components/containers/Main/Main'

import withNeeds from '../../../lib/withNeeds'

export default withNeeds(Main)
