import featuresService from '../../../server/lib/features-service'
import { initFeatures } from '../actions/featuresActions'
const { getFeatures } = featuresService

export default ({ store, req, brandConfig }) => {
  const { state: { featuresOverride } } = req
  store.dispatch(
    initFeatures(getFeatures(brandConfig), featuresOverride)
  )
}
