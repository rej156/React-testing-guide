let preRenderActions = []

if (!process.browser) {
  preRenderActions = [
    require('./setConfig').default,
    require('./localeDictionary').default,
    require('./featureFlags').default,
    require('./configAnalytics').default,
    require('./allowDebug').default,
    require('./setMediaType').default,
    require('./serverLocation').default
  ]
}

export default [...preRenderActions]
