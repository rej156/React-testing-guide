import { getLocaleDictionary } from '../../../lib/localisation'
import { setLocaleDictionary } from '../actions/localisationActions'

export default ({ store, brandConfig }) => {
  const { brandName, language } = brandConfig
  store.dispatch(setLocaleDictionary(getLocaleDictionary(language, brandName)))
}
