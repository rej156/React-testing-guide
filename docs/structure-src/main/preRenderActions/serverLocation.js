import { updateLocationServer } from '../actions/routingActions'

export default ({ store, req }) => {
  const { info: { hostname } } = req
  store.dispatch(updateLocationServer(req.url, hostname))
}
