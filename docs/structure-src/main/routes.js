import UI from './components/UI'

export default () => [
  { path: '/ui', exact: true, Component: UI }
]
