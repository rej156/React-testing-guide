export default [
  require('../../../../../../src/shared/sagas/common/ajaxCounterSaga').default,
  require('../../../../../../src/shared/sagas/common/analyticSaga').default,
  require('../../../../../../src/shared/sagas/common/dispatchApiSaga').default,
  require('../../../../../../src/shared/sagas/common/apiErrorSaga').default
]
