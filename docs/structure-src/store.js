import { compose, applyMiddleware, createStore, combineReducers } from 'redux'
import createSagaMiddleware, { END } from 'redux-saga'
import { apiMiddleware } from 'redux-api-middleware'
import ReduxAsyncQueue from 'redux-async-queue'
import thunkMiddleware from 'redux-thunk'
import localiseMiddleware from './lib/localise-middleware'
import { montyApiMiddleware } from './lib/preprocess-redux-api-middleware'

import core from './core'

export const initStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  const composeEnhancers = (
    process.env.NODE_ENV !== 'production' && typeof window === 'object'
  ) ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose

  const store = createStore(
    // reducers registered by features (./features/*/reducers)
    // initialState registered by:
    // - ./server/render
    // - ./browser/render
    combineReducers(core.get.reducers()), core.get.initialState(),
    // middleware order is important
    composeEnhancers(applyMiddleware(
      // additional middleware registered by features (./features/*/middleware)
      ...core.get.storeMiddleware(),
      montyApiMiddleware,
      apiMiddleware,
      thunkMiddleware,
      ReduxAsyncQueue,
      sagaMiddleware,
      localiseMiddleware
    ))
  )

  // For server-side rendering to render view after saga promises
  store.sagaPromise = sagaMiddleware.run(function* rootSaga() {
    // sagas registered by features (./features/*/sagas)
    yield core.get.sagas().map((startSaga) => startSaga())
  })
  store.close = () => store.dispatch(END)  // End Sagas

  core.register.store(store)

  return store
}
