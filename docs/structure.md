# Folder structure based on ideal Lerna submodules

<img src="../images/submodules.png" alt="Submodules" width="200px"/>

Basically the scenario is that using lerna we are able to split up our isomorphic React application into three different submodules/npm packages.

1. API - Http server + Data transformation layer (We don't need this at Lightful, thank God.); really just for isomrphic rendering.
2. Components - Idea is that this is where we put our 'dumb' components.

But oh wait with Lerna this means our components with their colocated files are essentially an open sourceable module! 
> "Lightful's open source React component library goes here" 
3. Main package goes here. Containers are essentially Features. Frontend business logic goes here.

***_Juicy stuff upcoming!_***

</br>


# Reasoning
<img src="../images/features.png" alt="Features" width="200px"/>

The idea is that everything is colocated under its feature, we bootstrap a registry object based on a features json configuration file.

> (Configuration file required for certain environments, default, dev, production)

Essentially feature flags!


# Application "start-up" process
0. _[App Core](./structure-src/core.js)_
1. _Renderer setup_
    - [bootstraps features](./structure-src/bootstrap.js)
        - exported modules are __registered to core__
            - routes, reducers, initial state, pre render actions, ...
    - [initialises store](./structure-src/store.js)
        - store is created with reducers, initial state, middleware, sagas __(registered by features)__
        - store is __registered to core__
    - initialises app with store & Router
        - [Browser](./structure-src/browser/render.js): [BrowserRouter](https://reacttraining.com/react-router/#browserrouter)
2. _[Application init](./structure-src/app.js)_
    - render Root component (registered by [Main feature](./structure-src/features/main/index.js))
        - [router object is __registered to core__](./structure-src/lib/withRootSetup.js)
    - render routes from route handlers __(registered by features)__
3. _Renderer render_
    - [Browser](./structure-src/browser/render.js): __(Webpack entry)__
        - react renders to DOM node
    - [Server](./structure-src/server/render.js): __(Hapi.js handler)__
        - dispatch pre render actions __(registered by features)__
        - initial react render to allow components to register needs to core
        - [fetch route needs](./structure-src/server/fetchNeeds.js) __([registered by components](./structure-src/lib/withNeeds.js))__
        - render view

## App Features
An app feature is a logical grouping of code/assets related to a specific area / domain of the application, each feature is responsible for its own:
- Routes
  - based on React Router v4
  - Top-Level route components register their own needs for server-side rendering
- Components & Styles
- Ducks - Reducers, Sagas, Actions, Selectors
> (I prefer Redux-Observable's Epics!)
- Initial / Persisted state
  - To be stored in local or session store
- Pre-render Actions (for SSR)
- ... (potentially could contain - analytics, errors, modals, ...)

### App features list:
- Main
    - Contains the root component / app UI shell
    - Responsible for common app requirements, i.e reducers, ...
- Home
    - This could really be moved into CMS feature
- CMS
- Product
    - Search
    - PLP
    - PDP
    - Find in Store
- Checkout
    - Shopping Bag
    - Collect From Store
- Account
    - Sign in
    - Register
    - Order History
- Store Locator
- ...

# Lerna scripts

<img src="../images/lerna scripts.png" alt="Submodules" />
