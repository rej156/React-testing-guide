# Unit Testing Monty
> In association with [Enzyme]() + [Jest](http://facebook.github.io/jest/) + __Good practices__.

## Test Coverage Checklist
- Output / Side-effects
  - [x] Cover all output & side-effects for conditions / branched logic
  - [x] Explicitly highlight all states for the unit of code
- Dependencies
  - [x] Stub / mock all dependencies - do not depend on their output or side-effects
  - [x] Cover any inputs (args/props) passed (especially non serializable - point 5)
  - [x] Cover the output / side-effects of any functions passed

## Snapshot Declarations
- I promise to...
  - [x] Review new and updated snapshots during development and in PRs
    - __Seriously!__ Check the snapshot outputs what I'm expecting
  - [x] Run `jest:watch` while developing
    - So I get prompted to update snapshots for components I've changed, and other cool things
    - And work a bit more TDD
  - [x] Where possible, reduce snapshots by only selecting what is being tested
    - `expect(getTreeFor(component.find('Something'))).toMatchSnapshot()`
    - `expect(component.find('Many').map(getTreeFor)).toMatchSnapshot()`
    - `expect(object.property).toMatchSnapshot()`

## Component Test Guide
> test = (props) + (Component) -> assert (render, lifecycle, events, calls)

- Tests the component in all possible states based on props, primarily asserting:
  1. Component Rendered output _(ideally only snapshot)_
    - for render output dependant on internal state, assert at the level the state update is triggered (options below)
  2. Component lifecycle _(if any)_
  3. DOM events _(if any)_
  4. Calls to dependencies _(if any)_
    - generally functions passed in props to children
- Use [test-component helper](../test/unit/helper/test-component) to reduce boilerplate and keep tests DRY
  - Vu Nam: But dude, this only does shallow rendering, what about `ref's` et al?
  - Helper: _(Assuming you trust React)_ You test them directly and mock the side-effects
    - `component.find('Something').node.ref(mockElement)` _Tada!_
    - ... or don't use the helper and use mount rendering (if you really have to!)
  - You: _(crickets)_
  - Helper: Come at me bro!

### Example Spec
```js
// import test dependencies
import testComponentHelper from '../../helpers/test-component'
// ...

// mock component dependencies - optional
const mockDepA = ...
jest.mock('dep-a', () => mockDepA)

// import component
import Component from 'path/to/Component'

describe('<Component />', () => {
  // set up reused mocks/stubs, test fixtures, helpers - if any
  const mockFoo = jest.fn(() => ...)
  const maybeInitialProps = {...}
  const renderComponent = testComponentHelper(Component, maybeRenderOptions)

  // 1. tests for all component & prop initial render possibilities
  describe('@renders', () => {
    // mostly snapshot tests
    it('in X state')
    it('with X prop')
  })

  // 2. test lifecycle methods - if any
  describe('@lifecycle', () => {
    describe('on componentWillMount', () => {
      it('calls A')
    })

    describe('on componentDidUpdate', () => {
      it('calls X when .../in ... state')
      it('updates state when .../in ... state')
    })
  })

  // 3. test DOM event handlers - if any
  describe('@events', () => {
    it('<button key="foo" /> calls C on click')
    it('<select key="bar" /> calls H on change')

    describe('<input key="baz" />', () => {
      it('calls X on focus')
      it('does not call X on focus when .../in ... state')
      it('updates state on mouseup')
    })
  })

  // 4a. test child component function props and refs - if any
  describe('@children', () => {
    describe('<select key="a" />', () => {
      it('sets X instance prop via ref')
    })

    describe('<Button key="b" />', () => {
      it('calls A on clickHandler prop')
    })
  })

  // 4x. Identify and add other common cases and document below
})
```

## Action Creator Test Guide
> test = (args) + (Action Creator) > assert (actions, calls)

- Tests the action dispatched from an action creator, primarily asserting:
  1. Dispatched actions
  2. calls to dependencies _(if any)_
- Only test action creators with any kind of logic (conditions, calls to dependancies, ...)
  - No value in testing really simple creators

## Reducer Test Guide
> test = (action, state) + (Reducer) -> assert (state)

- Tests the state outputted by a reducer for an action, primarily asserting:
  1. Outputted state

## _Feature Test Guide - WIP_
> test = (state, route, actions) + (Route, Dispatch, Store) -> assert (render, state, server, side-effects)

- Tests the integration of a feature within the application, only mocking externals to the application i.e browser I/O & server API, primarily asserting:
  1. Application Rendered output _(snapshot)_
  2. Application state _(snapshot)_
  3. Outbound only API requests _(if any)_
    - i.e order, payments, analytics, ...
  4. External side-effects _(if any)_
    - Browser I/O - i.e localstorage
    - Third-party API - i.e Klarna
    - ...

> This is what will eventually really help "invert the pyramid"!

## Migration path
### Tasks
- Use jest-codemods to do the initial heavy lifting & fix any issues / areas not modified correctly
- Replace use of `rewire` with `jest.mock`
- Replace use of `sinon.mock|spy` with `jest.fn` (optional)
  - but can't stub object.method with `jest.fn` i.e. `sinon.stub(foo, 'getBar') --> foo.getBar = jest.fn()`
- Refactor component tests to be inline with the [Component Test Guide](#component-test-guide)

### Once all tests are migrated
- Clean up unrequited tasks, script or dependencies

### Responsibilities
- Each developer
  - refactors / adds tests related to what they're working on
  - refactors N existing tests per X time period
- Team or a select few
  - Group the application into features (possibly using e2e features as a guide)
  - Write the specs for each feature
  - Create mocks / stubs for server, browser and external services
  - Implement the specs
