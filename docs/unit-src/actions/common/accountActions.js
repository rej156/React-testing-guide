import { MONTY_API } from '../../middleware/preprocess-redux-api-middleware'
import { setFormMeta } from '../../actions/common/formActions'

// Short user profile
export function userAccount(user) {
  return {
    type: 'USER_ACCOUNT',
    user
  }
}

export function closeCustomerDetailsModal() {
  return setFormMeta('customerDetails', 'modalOpen', false)
}

export function getAccount() {
  return {
    [MONTY_API]: {
      endpoint: '/account',
      method: 'get',
      overlay: true,
      typesPrefix: 'GET_USER_ACCOUNT',
      meta: {
        failure: {
          errorAlertBox: true
        }
      }
    }
  }
}

export function updateAccount(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/customerdetails',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'UPDATE_USER_ACCOUNT_FORM',
      formName: 'customerDetails',
      meta: {
        success: {
          formMeta: ['modalOpen', true]
        }
      }
    }
  }
}

export function changeShortProfileRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/shortdetails',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'CHANGE_SHORT_PROFILE_FORM',
      formName: 'customerShortProfile',
      meta: {
        success: {
          messageKey: 'Your profile details have been successfully updated.'
        }
      }
    }
  }
}

export function changePwdRequest(data, resetPassword) {
  return {
    [MONTY_API]: {
      endpoint: '/account/changepassword',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'CHANGE_PASSWORD_FORM',
      formName: 'changePassword',
      meta: {
        success: {
          dispatchApi: (resetPassword && getAccount()),
          messageKey: 'Your password has been successfully changed.'
        }
      }
    }
  }
}

export function setForgetPassword(value) {
  return {
    type: 'TOGGLE_FORGET_PASSWORD',
    value
  }
}

export function toggleForgetPassword() {
  return {
    type: 'TOGGLE_FORGET_PASSWORD'
  }
}

export function forgetPwdRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/forgetpassword',
      method: 'POST',
      body: data,
      overlay: true,
      typesPrefix: 'FORGOT_PASSWORD_FORM',
      formName: 'forgetPassword',
      meta: {
        success: {
          analytics: {
            type: 'AUTH',
            data,
            events: ['event109']
          }
        }
      }
    }
  }
}

const ORDER_HISTORY = '/account/order-history'

export function setOrderHistoryDetails(orderDetails) {
  return {
    type: 'SET_ORDER_HISTORY_DETAILS',
    orderDetails
  }
}

export function orderHistoryRequest() {
  return {
    [MONTY_API]: {
      endpoint: ORDER_HISTORY,
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_ORDER_HISTORY'
    }
  }
}

export function orderHistoryDetailsRequest(orderId) {
  return {
    [MONTY_API]: {
      endpoint: `${ORDER_HISTORY}/${orderId}`,
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_ORDER_HISTORY_DETAILS'
    }
  }
}
