import * as acc from '../accountActions'

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('Account Actions', () => {
  it('userAccount(user)', () => {
    snapshot(acc.userAccount('foo'))
  })

  it('closeCustomerDetailsModal()', () => {
    snapshot(acc.closeCustomerDetailsModal())
  })

  it('getAccount()', () => {
    snapshot(acc.getAccount())
  })

  it('updateAccount(data)', () => {
    snapshot(acc.updateAccount({}))
  })

  it('changeShortProfileRequest(data)', () => {
    snapshot(acc.changeShortProfileRequest({}))
  })

  it('changePwdRequest(data, resetPassword)', () => {
    snapshot(acc.changePwdRequest({}, true))
    snapshot(acc.changePwdRequest({}, false))
    snapshot(acc.changePwdRequest({}))
  })

  it('setForgetPassword(value)', () => {
    snapshot(acc.setForgetPassword('foo'))
  })

  it('toggleForgetPassword()', () => {
    snapshot(acc.toggleForgetPassword())
  })

  it('forgetPwdRequest(data)', () => {
    snapshot(acc.forgetPwdRequest({}))
  })

  it('orderHistoryRequest()', () => {
    snapshot(acc.orderHistoryRequest())
  })

  it('orderHistoryDetailsRequest(orderId)', () => {
    snapshot(acc.orderHistoryDetailsRequest('foo'))
  })
})
