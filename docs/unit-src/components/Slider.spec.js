import { pick } from 'ramda'
import testComponentHelper from 'test/unit/helpers/test-component'
import Slider from './Slider'

describe('<Slider/>', () => {
  const initialProps = {
    minValue: 0,
    maxValue: 100,
    viewportWidth: 320
  }

  const sliderProperties = [
    'slider', 'iconWidth',
    'height', 'width', 'left',
    'offsetAllowance',
    'handlerStartOffset',
    'isHandlerTooFar'
  ]

  const renderComponent = testComponentHelper(Slider.WrappedComponent)

  const iconWidth = 25
  const iconNode = { getBoundingClientRect: () => ({ width: iconWidth }) }
  const sliderNode = { getBoundingClientRect: jest.fn(() => ({ height: 4, width: 1000, left: 0, top: 0 })) }

  afterEach(() => {
    sliderNode.getBoundingClientRect.mockClear()
  })

  describe('@constructor', () => {
    it('sets initial state', () => {
      expect(renderComponent(initialProps).instance.state).toMatchSnapshot()
    })

    it('sets initial slider properties', () => {
      expect(pick(sliderProperties, renderComponent(initialProps).instance)).toMatchSnapshot()
    })

    it('sets handle press event handlers', () => {
      expect(renderComponent(initialProps).instance.onPressEventHandlers).toMatchSnapshot()
    })
  })

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    describe('on componentDidMount', () => {
      const filterAddListenerCalls = (filterCall) => global.document
        .addEventListener.mock.calls.filter(filterCall)

      beforeEach(() => renderComponent(initialProps).instance.componentDidMount())

      it('adds touch event listeners', () => {
        expect(filterAddListenerCalls(([name]) => /^touch/.test(name))).toMatchSnapshot()
      })

      it('adds mouse event listeners', () => {
        expect(filterAddListenerCalls(([name]) => /^mouse/.test(name))).toMatchSnapshot()
      })
    })

    describe('on componentWillUnmount', () => {
      const filterRemoveListenerCalls = (filterCall) => global.document
        .removeEventListener.mock.calls.filter(filterCall)

      beforeEach(() => renderComponent(initialProps).instance.componentWillUnmount())

      it('removes touch event listeners', () => {
        expect(filterRemoveListenerCalls(([name]) => /^touch/.test(name))).toMatchSnapshot()
      })

      it('removes mouse event listeners', () => {
        expect(filterRemoveListenerCalls(([name]) => /^mouse/.test(name))).toMatchSnapshot()
      })
    })

    describe('on componentWillRecieveProps', () => {
      let renderedComponent

      beforeEach(() => {
        renderedComponent = renderComponent(initialProps)
        const { wrapper } = renderedComponent

        wrapper.find('.Slider-icon').node.ref(iconNode)
        wrapper.find('.Slider').node.ref(sliderNode)
      })

      it('updates all .Slider-handle styles on getBoundingClientRect changes', () => {
        const { wrapper } = renderedComponent
        const getStyleProp = (componentWrapper) => componentWrapper.prop('style')
        const before = wrapper.find('.Slider-handle').map(getStyleProp)

        sliderNode.getBoundingClientRect.mockReturnValueOnce({ width: 100, left: 50 })
        wrapper.setProps()

        const after = wrapper.find('.Slider-handle').map(getStyleProp)
        expect({ before, after }).toMatchSnapshot()
      })

      it('updates only .Slider-label--minHandle renders on minValue changes', () => {
        const { wrapper, getTreeFor } = renderedComponent
        const before = wrapper.find('.Slider-label').map(getTreeFor)

        wrapper.setProps({ minValue: 10 })

        const after = wrapper.find('.Slider-label').map(getTreeFor)
        expect({ before, after }).toMatchSnapshot()
      })

      it('updates only .Slider-label--maxHandle renders on maxValue changes', () => {
        const { wrapper, getTreeFor } = renderedComponent
        const before = wrapper.find('.Slider-label').map(getTreeFor)

        wrapper.setProps({ maxValue: 50 })

        const after = wrapper.find('.Slider-label').map(getTreeFor)
        expect({ before, after }).toMatchSnapshot()
      })

      it('does not update .Slider render when only viewportWidth changes', () => {
        const { wrapper, getTreeFor } = renderedComponent
        const previousTree = getTreeFor(wrapper.find('.Slider'))

        wrapper.setProps({ viewportWidth: 720 })

        const currentTree = getTreeFor(wrapper.find('.Slider'))
        expect(currentTree).toMatchObject(previousTree)
        expect(currentTree).toMatchSnapshot()
      })
    })
  })

  describe('@events', () => {
    const currentTarget = { getBoundingClientRect: jest.fn(() => ({ left: 0 })) }

    const getMockHandlerPosition = jest.fn(() => ({ pageX: 0, pageY: 0 }))
    const setMockHandlerPosition = (pageX = 0, pageY = 0) => getMockHandlerPosition
      .mockReturnValueOnce({ pageX, pageY })

    const getEvent = (eventName) => /^touch/.test(eventName)
      ? { currentTarget, changedTouches: [getMockHandlerPosition()] }
      : { currentTarget, ...getMockHandlerPosition() }

    const assertHandleSnapshot = ({ wrapper, getTreeFor }) => {
      expect(wrapper.find('.Slider-handle').map(getTreeFor)).toMatchSnapshot()
    }
    const assertSliderSnapshot = ({ wrapper, getTreeFor }) => {
      expect(getTreeFor(wrapper.find('.Slider'))).toMatchSnapshot()
    }

    beforeEach(() => {
      currentTarget.getBoundingClientRect.mockClear()
      getMockHandlerPosition.mockClear()
    })

    describe('handle press', () => {
      describe('sets active .Slider-handle', () => {
        const eventTests = ['touchstart', 'mousedown']
        eventTests.forEach((eventName) => {
          it(`on ${eventName}`, (done) => {
            renderComponent(initialProps).simulateEvent({
              eventName, getEvent, eventType: 'element',
              targets: [
                { selector: '.Slider-handle--minHandle', afterEvent: assertHandleSnapshot },
                { selector: '.Slider-handle--maxHandle', afterEvent: assertHandleSnapshot }
              ],
              afterAllEvents: done
            })
          })
        })
      })

      describe('sets handler start offset', () => {
        const assertStartOffset = (expectedOffset) => ({ instance }) => {
          expect(instance.handlerStartOffset).toEqual(expectedOffset)
        }

        const eventTests = [{
          eventName: 'touchstart',
          targetRectLeft: { min: 5, max: 20 },
          handlerPositionX: { min: 15, max: 30 }
        }, {
          eventName: 'mousedown',
          targetRectLeft: { min: 2, max: 15 },
          handlerPositionX: { min: 12, max: 25 }
        }]

        eventTests.forEach(({ eventName, targetRectLeft, handlerPositionX }) => {
          it(`on ${eventName}`, (done) => {
            renderComponent(initialProps).simulateEvent({
              eventName, getEvent, eventType: 'element',
              targets: [{
                selector: '.Slider-handle--minHandle',
                beforeEvent: () => {
                  currentTarget.getBoundingClientRect
                    .mockReturnValueOnce({ left: targetRectLeft.min })
                  setMockHandlerPosition(handlerPositionX.min)
                },
                afterEvent: assertStartOffset(handlerPositionX.min - targetRectLeft.min)
              }, {
                selector: '.Slider-handle--maxHandle',
                beforeEvent: () => {
                  currentTarget.getBoundingClientRect
                    .mockReturnValueOnce({ left: targetRectLeft.max })
                  setMockHandlerPosition(handlerPositionX.max)
                },
                afterEvent: assertStartOffset(handlerPositionX.max - targetRectLeft.max)
              }],
              afterAllEvents: done
            })
          })
        })
      })
    })

    xdescribe('handle move - after handle press', () => {
      let renderedComponent

      const onChangeFinished = jest.fn()
      const sliderWidth = 100
      const availableSliderWidth = sliderWidth - iconWidth * 2

      const testHandleMove = (eventTests, onComplete = jest.fn()) => eventTests.forEach(({
        eventName, moveHandlerTo, handlerPositionY = {}, afterEachTargetEvent = jest.fn()
      }) => {
        it(`on ${eventName}`, (done) => {
          renderedComponent.simulateEvent({
            eventName, getEvent, eventType: 'document',
            targets: [{
              beforeEvent: ({ instance }) => {
                currentTarget.getBoundingClientRect
                  .mockReturnValueOnce({ left: 0 })
                instance.onHandlePress({ currentTarget, pageX: 0, pageY: 0 }, 'minHandle')
                setMockHandlerPosition(moveHandlerTo.min, handlerPositionY.min)
              },
              afterEvent: afterEachTargetEvent
            }, {
              beforeEvent: ({ instance }) => {
                currentTarget.getBoundingClientRect
                  .mockReturnValueOnce({ left: sliderWidth - iconWidth })
                instance.onHandlePress({ currentTarget, pageX: availableSliderWidth, pageY: 0 }, 'maxHandle')
                setMockHandlerPosition(moveHandlerTo.max, handlerPositionY.max)
              },
              afterEvent: afterEachTargetEvent
            }],
            afterAllEvents: (component) => {
              onComplete(component)
              done()
            }
          })
        })
      })

      beforeEach(() => {
        renderedComponent = renderComponent({ ...initialProps, onChangeFinished })
        const { wrapper } = renderedComponent

        sliderNode.getBoundingClientRect
          .mockReturnValueOnce({ width: sliderWidth, left: 0 })

        wrapper.find('.Slider-icon').node.ref(iconNode)
        wrapper.find('.Slider').node.ref(sliderNode)
      })

      describe('updates .Slider-label values and .Slider-handle position', () => {
        testHandleMove([{
          eventName: 'touchmove',
          moveHandlerTo: { min: availableSliderWidth * 0.05, max: availableSliderWidth * 0.5 },
          afterEachTargetEvent: assertSliderSnapshot
        }, {
          eventName: 'mousemove',
          moveHandlerTo: { min: availableSliderWidth * 0.25, max: availableSliderWidth * 0.75 },
          afterEachTargetEvent: assertSliderSnapshot
        }])
      })

      describe('sets .Slider-label to min/max value when handler attempt to exceed min/max', () => {
        testHandleMove([{
          eventName: 'touchmove',
          moveHandlerTo: { min: -availableSliderWidth * 0.05, max: availableSliderWidth * 1.5 },
          afterEachTargetEvent: assertSliderSnapshot
        }, {
          eventName: 'mousemove',
          moveHandlerTo: { min: -availableSliderWidth * 0.25, max: availableSliderWidth * 1.75 },
          afterEachTargetEvent: assertSliderSnapshot
        }])
      })

      describe('sets .Slider-label to min value when handler attempt to overlap .Slider-handle', () => {
        testHandleMove([{
          eventName: 'touchmove',
          moveHandlerTo: { min: availableSliderWidth * 0.5, max: availableSliderWidth * 0.05 },
          afterEachTargetEvent: assertSliderSnapshot
        }, {
          eventName: 'mousemove',
          moveHandlerTo: { min: availableSliderWidth * 0.75, max: availableSliderWidth * 0.25 },
          afterEachTargetEvent: assertSliderSnapshot
        }])
      })

      describe('does not update .Slider when handler is too far vertically', () => {
        const sliderTop = 100
        const sliderHeight = 4

        beforeEach(() => {
          sliderNode.getBoundingClientRect
            .mockReturnValue({ width: sliderWidth, height: sliderHeight, top: sliderTop, left: 0 })

          renderedComponent.wrapper.find('.Slider').node.ref(sliderNode)
        })

        testHandleMove([{
          eventName: 'touchmove',
          moveHandlerTo: { min: availableSliderWidth * 0.5, max: availableSliderWidth * 0.5 },
          handlerPositionY: { min: sliderTop, max: sliderTop },
          afterEachTargetEvent: (component) => {
            setMockHandlerPosition(availableSliderWidth * 0)
            component.instance.onHandleMove(getEvent('touchmove'))
            assertSliderSnapshot(component)
          }
        }, {
          eventName: 'mousemove',
          moveHandlerTo: { min: availableSliderWidth * 0.75, max: availableSliderWidth * 0.75 },
          handlerPositionY: { min: sliderTop, max: -sliderTop * 2.5 },
          afterEachTargetEvent: (component) => {
            setMockHandlerPosition(availableSliderWidth * 1)
            component.instance.onHandleMove(getEvent('mousemove'))
            assertSliderSnapshot(component)
          }
        }])
      })
    })

    xdescribe('handle release - after handle press', () => {
      describe('clears active .Slider-handle', () => {
        it('on touchend', () => {})
        it('on mouseup', () => {})
      })
      describe('sets handler too far to false', () => {
        it('on touchend', () => {})
        it('on mouseup', () => {})
      })
      describe('calls on change finished with min & max value', () => {
        it('on touchend', () => {})
        it('on mouseup', () => {})
      })
    })
  })

  describe('@elements', () => {
    it('.Slider updates instance and state on ref', () => {
      const { wrapper, instance } = renderComponent(initialProps)
      const instanceProperties = [...sliderProperties, 'state']
      const before = pick(instanceProperties, instance)

      instance.icon = { getBoundingClientRect: () => ({ width: 10 }) }
      wrapper.find('.Slider').node.ref(sliderNode)

      const after = pick(instanceProperties, instance)
      expect({ before, after }).toMatchSnapshot()
    })

    it('.Slider-handle sets handle on ref', () => {
      const { wrapper, instance } = renderComponent(initialProps)
      wrapper.find('.Slider-handle--minHandle').node.ref('min')
      wrapper.find('.Slider-handle--maxHandle').node.ref('max')
      expect([instance.minHandle, instance.maxHandle]).toEqual(['min', 'max'])
    })

    it('.Slider-icon sets icon on ref', () => {
      const { wrapper, instance } = renderComponent(initialProps)
      wrapper.find('.Slider-icon').node.ref('min')
      expect(instance.icon).toBe('min')
    })
  })
})
