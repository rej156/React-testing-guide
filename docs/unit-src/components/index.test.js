require('mock-local-storage')
require('babel-polyfill')
require('babel-register')({
  plugins: ['rewire']
})
require('colors')
const jsdom = require('jsdom')
const nock = require('nock')
const google = require('../mocks/google')
const HTML = '<html><body></body></html>'
const { setDictionaries } = require('../../src/shared/lib/localisation')
const dictionaries = require('../../src/shared/constants/dictionaries').default

setDictionaries(dictionaries)
global.document = jsdom.jsdom(HTML)
global.window = document.defaultView
global.Event = document.defaultView.Event
global.navigator = Object.assign(window.navigator, { onLine: true })
global.process.env.BV_SHAREDKEY = 'asdf'
global.process.env.JWT_SECRET = 'test123'
global.process.browser = false
global.window.google = google
global.process.env.REDIS_URL = ''

nock.emitter.on('no match', (req) => {
  console.log(`Request not mocked for path: ${req.path}`.red) // eslint-disable-line
  process.exit(-1)
})

process.on('unhandledRejection', (error) => console.log(error)) // eslint-disable-line
