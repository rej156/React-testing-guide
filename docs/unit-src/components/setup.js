require('mock-local-storage')
require('colors')

const nock = require('nock')
const google = require('../mocks/google')
const { setDictionaries } = require('../../src/shared/lib/localisation')
const dictionaries = require('../../src/shared/constants/dictionaries').default

setDictionaries(dictionaries)
global.process.env.BV_SHAREDKEY = 'asdf'
global.process.env.JWT_SECRET = 'test123'
global.process.env.REDIS_URL = ''
global.process.browser = false

Object.defineProperty(window.navigator, "onLine", { value: () => true })
Object.defineProperty(window, "google", { value: () => google })

nock.emitter.on('no match', (req) => {
  console.log(`Request not mocked for path: ${req.path}`.red) // eslint-disable-line
  process.exit(-1)
})

process.on('unhandledRejection', (error) => console.log(error)) // eslint-disable-line
