import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

const mockLocalise = (value, ...expressions) => {
  let index = 0
  return [].concat(value)
    .join(expressions.length ? '${}' : '')
    .replace(/\${}/g, () => expressions[index++])
}

const mockPrice = (value) => `£${parseFloat(value).toFixed(2)}`

const simulateEventHelper = (component) => ({
  eventName, eventType, targets = [],
  getEvent = jest.fn(),
  afterAllEvents = jest.fn()
}) => {
  targets.forEach(({ selector, beforeEvent = jest.fn(), afterEvent = jest.fn() }) => {
    component.instance.componentDidMount()
    beforeEvent(component)

    const event = getEvent(eventType)

    if (eventType === 'element' && selector) {
      component.wrapper.find(selector)
        .simulate(eventName, event)
    }
    if (eventType === 'document') {
      const [, eventHandler] = global.document.addEventListener.mock.calls
        .find(([listenerName]) => listenerName === eventName)

      eventHandler(event)
    }

    afterEvent(component)
  })

  afterAllEvents(component)
}

const renderComponentHelper = (Component, mountOptions = {}) => {
  beforeAll(() => {
    global.document.addEventListener = jest.fn(
      global.document.addEventListener.bind(global.document)
    )

    global.document.removeEventListener = jest.fn(
      global.document.removeEventListener.bind(global.document)
    )
  })

  afterEach(() => {
    global.document.addEventListener.mockClear()
    global.document.removeEventListener.mockClear()
  })

  return ({ children, ...props } = {}) => {
    const mountCommonContext = { context: {
      // mock localise lib
      l: jest.fn(mockLocalise),
      // mock price lib
      p: jest.fn(mockPrice)
    } }

    const wrapper = shallow(
      <Component {...props}>{ children }</Component>,
      { ...mountCommonContext, ...mountOptions }
    )

    const component = {
      wrapper,
      instance: wrapper.instance(),
      getTree: () => toJson(wrapper),
      getTreeFor: (selectedWrapper) => toJson(selectedWrapper)
    }

    return {
      ...component,
      simulateEvent: simulateEventHelper(component)
    }
  }
}

export default renderComponentHelper
