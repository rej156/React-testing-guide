import { error } from './dev-logger'

let _dictionary
let dictionaries

const languageMap = {
  'en-gb': 'en-GB',
  'en-us': 'en-US',
  'en-eu': 'en-GB',
  'en-sg': 'en-GB',
  'en-my': 'en-GB',
  'en-th': 'en-GB',
  'en-id': 'en-GB',
  'de-DE': 'de-DE',
  'fr-fr': 'fr-FR'
}

const interpolate = (string, expressions) => {
  if (typeof string === 'string') {
    let index = 0
    return string.replace(/\${}/g, () => expressions[index++])
  }
}

/**
 * Tag function to localise strings
 * usages:
 *   localise`product ${productName} added to bag`
 *   localise`some string without expressions`
 *   localise('some string without expressions')
 */
export function localise(language, brandName, strings, ...expressions) {
  const dictionary = _dictionary || (dictionaries[brandName] && dictionaries[brandName][languageMap[language]])

  if (!dictionary) {
    return (typeof strings === 'string') ? strings : strings[0]
  }

  let translation
  let key
  if (!expressions.length) {
    key = strings
    translation = (typeof strings === 'string') ? dictionary[strings] : dictionary[strings[0]]
  } else {
    key = strings.join('${}')
    translation = interpolate(dictionary[key] || key, expressions)
  }

  if (!translation) {
    if (process.env.SHOW_MISSING_LOCALISATION) error(`No localisation found for: '${key}'`)  // eslint-disable-line no-console
    return (typeof strings === 'string') ? strings : strings[0]
  }

  return translation
}

export function getLocaleDictionary(locale, brand) {
  return dictionaries[brand][languageMap[locale]]
}

// Server side
export function setDictionaries(dicts) {
  if (process.browser) {
    throw new Error('Dictionaries cannot be set client side')
  }
  dictionaries = dicts
}

// Client side
export function setDictionary(dict) {
  if (!process.browser) {
    throw new Error('Dictionary cannot be set server side')
  }
  _dictionary = dict
}
