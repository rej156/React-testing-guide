import { path, lensPath, view, set } from 'ramda'
import { localise } from './localisation'

export function localiser(getState) {
  const { language, brandName } = getState().config
  return localise.bind(null, language, brandName)
}

function ensureArray(a) {
  return Array.isArray(a) ? a : [a]
}

function makeLocal(l, fields, obj) {
  function translate(field) {
    const lens = lensPath(field.split('.'))
    const text = view(lens, obj)

    if (text) {
      obj = set(lens, l(text), obj)
    }
  }

  ensureArray(fields).forEach(translate)

  return obj
}

function process(getState, action) {
  const ml = makeLocal.bind(null, localiser(getState), action.meta.localise)

  if (action.payload) action.payload = ml(action.payload) // FSA
  else action = ml(action)

  return action
}

export default ({ getState }) => (next) => (action) => {
  return next(
    path(['meta', 'localise'], action) ? process(getState, action) : action
  )
}
