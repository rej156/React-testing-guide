import { CALL_API } from 'redux-api-middleware'

const MONTY_API = Symbol('Call Arcdia API')
const apiTypes = ['REQUEST', 'SUCCESS', 'FAILURE']
const prefix = (!!process.browser) ? '' : 'http://localhost:3000'

function setEndpoint(path) {
  if (typeof path !== 'string') {
    throw new TypeError('Specify a string endpoint URL')
  }

  return (path.includes('https')) ? path : `${prefix}/api${path}`
}

function ensureString(body) {
  return typeof body === 'object' ? JSON.stringify(body) : body
}

function getCodeFromState(state) {
  return state.config.storeCode
}

function setBrandHeader(state) {
  return { 'BRAND-CODE': getCodeFromState(state) }
}

function setCookieHeader({ auth: { token } }) {
  return !process.browser && token ? { cookie: `token=${token}` } : {}
}

function attachHeaders(headers = {}, state) {
  return Object.assign(
    headers,
    { 'Content-Type': 'application/json' },
    setBrandHeader(state),
    setCookieHeader(state)
  )
}

function ensureObjects(data = {}) {
  return {
    all: data.all || {},
    each: [
      data.request || {},
      data.success || {},
      data.failure || {}
    ]
  }
}

function createTypes(action) {
  const meta = ensureObjects(action.meta)

  meta.all.overlay = !!action.overlay
  meta.all.requestUrl = setEndpoint(action.endpoint)
  if (action.formName) meta.all.formName = action.formName

  return apiTypes.map((apiType, i) => ({
    type: `${action.typesPrefix}_API_${apiType}`,
    meta: Object.assign({}, meta.all, meta.each[i])
  }))
}

function createNewAction(action, state) {
  return {
    body: ensureString(action.body),
    credentials: 'same-origin',
    endpoint: setEndpoint(action.endpoint),
    headers: attachHeaders(action.headers, state),
    types: createTypes(action)
  }
}

function processApiRequest(action, state) {
  const newAction = createNewAction(action, state)

  delete action.formName
  delete action.meta
  delete action.overlay
  delete action.typesPrefix

  return Object.assign(action, newAction)
}

const montyApiMiddleware = ({ getState }) => (next) => (action) => {
  return next(
    action[MONTY_API] ?
      { [CALL_API]: processApiRequest(action[MONTY_API], getState()) } :
      action
  )
}

export {
  MONTY_API,
  montyApiMiddleware
}
