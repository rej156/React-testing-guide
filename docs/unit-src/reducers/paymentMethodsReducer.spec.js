import testReducer from '../paymentMethodsReducer'

const snapshot = (reducer) => expect(reducer).toMatchSnapshot()

describe('Payment Method Reducer', () => {
  describe('GET_PAYMENT_METHODS_API_SUCCESS', () => {
    it('stores payload in state', () => {
      snapshot(testReducer([], {
        type: 'GET_PAYMENT_METHODS_API_SUCCESS',
        payload: ['foo']
      }))
    })
  })

  describe('GET_PAYMENT_METHODS_API_FAILURE', () => {
    it('clears state', () => {
      snapshot(testReducer(['foo', 'bar'], {
        type: 'GET_PAYMENT_METHODS_API_FAILURE',
        payload: {
          message: 'It failed'
        },
        error: true
      }))
    })
  })

  describe('DEL_PAYMENT_METHODS_API_SUCCESS', () => {
    it('stores payload in state', () => {
      snapshot(testReducer(['foo', 'bar'], {
        type: 'DEL_PAYMENT_METHODS_API_SUCCESS',
        payload: ['foo']
      }))
    })
  })
})
