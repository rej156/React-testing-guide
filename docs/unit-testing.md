# What and how to unit test

So here we ideally Jest snapshot everything.
Why?

Depends how lazy you are, if you haven't done TDD then a match snapshot should suffice as documentation of function output.

This allows lazy code coverage testing and prevents any further developer from breaking your logic by accident code wise!

Otherwise, do real TDD and actually write your assertions as you go along.
> A good example is the [core.js spec](./structure-src/__test__/core.spec.js) and the [bootstrap.js spec](./structure-src/__test__/bootstrap.spec.js).

I get it, yes your assertions during TDD are basically your snapshots sometimes too. :)

Essentially the above is a lib function being TDD'd!

## [Action testing](./unit-src/actions/common/)

***Please note that this is not the ideal folder structure!***

***The common folder represents actions colocated with their component***

(A dumb component; i.e. An open sourceable one!)

This is kinda boring but basically your snapshots should capture how an action is transformed with your custom Redux epic middlewares in place.

## [Middleware testing](./unit-src/middleware/)

Here we dispatch a mock typed action object as an argument to the middleware and we snapshot.

The snapshots exemplify that the middlewares transform the actions as their own logic intends.
Along with potentially asserting what other actions are potentially dispatched upon receiving an action that it is supposed to listen for.

In my opinion epics/sagas are middleware too but we can separate logical concerns into their own folders.

## [Reducers testing](./unit-src/reducers/)

> Would like to note that in the source code, it references our createReducer function which is like below:

~~~
export function createReducer(initialState, handlers) {
   return function reducer(state, action) {
      const newState = (state === undefined) ? initialState : state if (handlers.hasOwnProperty(action.type)) {
         return handlers[action.type](newState, action) 
      }
      return newState 
  }
}
~~~
Used like so.

~~~
import { createReducer } from '../../lib/create-reducer'

const initialState = []

export default createReducer(initialState, {
  GET_PAYMENT_METHODS_API_SUCCESS: (state, { payload }) => payload,
  GET_PAYMENT_METHODS_API_FAILURE: () => [],
  DEL_PAYMENT_METHODS_API_SUCCESS: (state, { payload }) => payload
})
~~~

## [Components testing](./unit-src/components/Slider.spec.js)

Too long to describe, I'll let the code speak for itself. :)

Typical list 
1. Initial state
2. It renders
3. Lifecycle hooks
4. DOM events
5. Elements/refs

[Component unit test helper](./unit-src/components/test-component.js)

> If you're wondering about the mountCommonContext function in the helper, we have two global component functions called l and p, they both localise text and format pricing according to the country/app configuration the user has set it to.

They're like so using the lovely function template string argument ES6 syntax!
~~~
render() {
  const { l, p } = this.context
  return (<p>{l`Localised string`}</p>)
}
~~~

>The simulateEventHelper function differentiates between asserting document event listener calls or enzyme specific .simulate('click/...') calls.

# P.S. Here's our internal testing [guide](./testy-monty.md)

This is actually much better than all the above... SORRY!